$(document).ready(function() {

    $('#thumb1').click(function () {
        if($('body').hasClass('gallery-mobile')){
          $('.image_wrapper img').attr('src','img/gallery/1.jpg');
        } else {
          $('#fixed_bg img').attr('src','img/gallery/1.jpg');
        }
    });
    $('#thumb2').click(function () {
      if($('body').hasClass('gallery-mobile')){
        $('.image_wrapper img').attr('src','img/gallery/2.jpg');
      } else {
      	$('#fixed_bg img').attr('src','img/gallery/2.jpg');
      }
    });
    $('#thumb3').click(function () {
      if($('body').hasClass('gallery-mobile')){
        $('.image_wrapper img').attr('src','img/gallery/3.jpg');
      } else {
      	$('#fixed_bg img').attr('src','img/gallery/3.jpg');
      }
    });
    $('#thumb4').click(function () {
      if($('body').hasClass('gallery-mobile')){
        $('.image_wrapper img').attr('src','img/gallery/4.jpg');
      } else {
      	$('#fixed_bg img').attr('src','img/gallery/4.jpg');
      }
    });
    $('#thumb5').click(function () {
      if($('body').hasClass('gallery-mobile')){
        $('.image_wrapper img').attr('src','img/gallery/5.jpg');
      } else {
      	$('#fixed_bg img').attr('src','img/gallery/5.jpg');
      }
    });

    //selected image border control
    $('#thumb_list a').click(function() {
      $('#thumb_list img').removeClass('active'); //removes class from all imgs
      $('img', this).addClass('active'); //adds to current
    });


    //check width of window
    //if width < 880, rename .gallery from body to .gallery-mobile
    //remember to revert if > than 880
    var $window = $(window);
    var $pane = $('#pane1');

    function checkWidth() {
        var windowsize = $window.width();

        //change bg image depending on size
        if (windowsize > 768) {
          if($('body').hasClass('gallery-mobile')){ //runs once, prevents flickering
            $('body').removeClass('gallery-mobile');
            $('body').addClass('gallery');
            $('#fixed_bg img').attr('src','img/gallery/1.jpg');
            //$('.gallery').css('background-image', 'url(img/gallery/1.jpg)');
          }else{
            return;
          }
        } else {
          if($('body').hasClass('gallery')){
            $('body').removeClass('gallery');
            $('body').addClass('gallery-mobile');
            $('#fixed_bg img').attr('src','img/gallery_bg_mobile.jpg');
            //$('.gallery-mobile').css('background-image', 'url(img/gallery_bg_mobile.png)');
          }else{
            return;
          }
        }


    }
    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);


    //change a href link for "PHOTOS"
    //open fancybox if desktop
    //open gallery.html if mobile
    $('a#gallery').click(function(e){
      if ($(window).width() < 769){
        $('a#gallery').attr('href','gallery.html');
        console.log("open gallery.html :" + $(window).width() );
      }else{
        $('a#gallery').attr('href','javascript:;');
        console.log("open fancybox :" +$(window).width() );
      }
    });

    //fancybox open gallery
    $("a#gallery").click(function() {
      $.fancybox.open([
        {
          href : 'img/gallery/1.jpg',
        }, {
          href : 'img/gallery/2.jpg',
        }, {
          href : 'img/gallery/3.jpg',
        }, {
          href : 'img/gallery/4.jpg',
        }, {
          href : 'img/gallery/5.jpg',
        }
      ], {
        helpers : {
          thumbs : {
            width: 120,
            height: 120
          }
        }
      });
    });


});